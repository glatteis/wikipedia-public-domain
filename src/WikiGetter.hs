{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
module WikiGetter
  ( getPublicDomainImage
  )
where

import           GHC.Generics                   ( Generic )
import           Network.Wreq
import qualified Data.Text                     as T
import           Data.Aeson
import           Data.Aeson.Types
import           Control.Lens.Getter
import           Control.Monad.Catch
import           Data.HashMap.Strict
import           Network.URI.Encode             ( encodeText )
import           Data.Vector                    ( head )

randomURL :: String
randomURL =
  "https://commons.wikimedia.org/w/api.php?action=query&list=random&rnnamespace=6&rnlimit=100&format=json"
infoURL :: String
infoURL =
  "https://commons.wikimedia.org/w/api.php?action=query&prop=imageinfo&iiprop=url|extmetadata&format=json&titles="

data RandomImage = RandomImage {
    id :: Int,
    ns :: Int,
    title :: T.Text
} deriving (Show, Generic)
instance FromJSON RandomImage

newtype RandomResponse = RandomResponse {
    random :: [RandomImage]
} deriving (Show, Generic)
instance FromJSON RandomResponse

data WikipediaReponse a = WikipediaReponse {
    batchcomplete :: T.Text,
    query :: a
} deriving (Show, Generic)
instance FromJSON (WikipediaReponse RandomResponse)

getRandomImages :: IO (Maybe [RandomImage])
getRandomImages = catch
  (do
    response <- asJSON =<< get randomURL
    let result :: WikipediaReponse RandomResponse = response ^. responseBody
    pure . Just . random $ query result
  )
  (\(e :: JSONError) -> do
    print e
    pure Nothing
  )

getPublicDomainImage :: IO (Maybe T.Text)
getPublicDomainImage = do
  maybeImages <- getRandomImages
  case maybeImages of
    Just images -> getPublicDomainImageFromList images
    Nothing     -> pure Nothing

getPublicDomainImageFromList :: [RandomImage] -> IO (Maybe T.Text)
getPublicDomainImageFromList (x : xs) = do
  url <- urlIfPublicDomain x
  case url of
    Just image -> pure $ Just image
    Nothing    -> getPublicDomainImageFromList xs
getPublicDomainImageFromList [] = pure Nothing

-- returns an image url if the image is public domain, else returns Nothing
urlIfPublicDomain :: RandomImage -> IO (Maybe T.Text)
urlIfPublicDomain image = catch
  (do
    let url = infoURL ++ (T.unpack . encodeText $ title image)
    response <- asJSON =<< get url
    let result :: Object = response ^. responseBody
    let pages :: Maybe Object = parseMaybe
          (\x -> do
            q <- x .: "query"
            q .: "pages"
          )
          result
    case pages of
      Nothing -> pure Nothing
      Just m  -> if Data.HashMap.Strict.null m
        then pure Nothing
        else case Data.HashMap.Strict.lookup (Prelude.head $ keys m) m of
          Nothing             -> pure Nothing
          Just (Object value) -> do
            let imageinfo :: Maybe Array = parseMaybe (.: "imageinfo") value
            case imageinfo of
              Just arr -> case Data.Vector.head arr of
                (Object obj) -> do
                  let extmetadata :: Maybe Object =
                        parseMaybe (.: "extmetadata") obj
                  let licenseInfo :: Maybe T.Text =
                        parseMaybe (.: "value")
                          =<< parseMaybe (.: "License")
                          =<< extmetadata
                  let imageURL :: Maybe T.Text =
                        parseMaybe (.: "url") =<< pure obj
                  print licenseInfo
                  case licenseInfo of
                    Just "cc0" -> pure imageURL
                    Just "pd"  -> pure imageURL
                    _          -> pure Nothing
                _ -> pure Nothing
              _ -> pure Nothing
          _ -> pure Nothing
  )
  (\(e :: JSONError) -> do
    print e
    pure Nothing
  )
